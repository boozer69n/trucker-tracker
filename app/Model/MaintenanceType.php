<?php
App::uses('AppModel', 'Model');
/**
 * MaintenanceType Model
 *
 */
class MaintenanceType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
