<?php
App::uses('AppModel', 'Model');
/**
 * Invoice Model
 *
 * @property Customer $Customer
 * @property LineItem $LineItem
 */
class Invoice extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';
	public $virtualFields = array('total' => 'SELECT SUM(price) FROM line_items WHERE invoice_id=Invoice.id');

    var $validate = array(
        'customer_id' => array(
            'cust-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Customer must be selected'
            )
        )
    );


    /**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'customer_id'
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'LineItem' => array(
			'className' => 'LineItem',
			'foreignKey' => 'invoice_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
