<?php
App::uses('AppModel', 'Model');
/**
 * Equipment Model
 *
 * @property Employee $Employee
 */
class Equipment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'plate';

    var $validate = array(
        'make' => array(
            'make-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a make'
            )
        ),
        'model' => array(
            'model-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a model'
            )
        ),
        'year' => array(
            'year-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a year'
            )
        ),
        'plate' => array(
            'plate-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a license plate'
            )
        ),
        'employee_id' => array(
            'empid-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must select an employee'
            )
        )

    );

    /**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id'
		),
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'employee_id'
		)
	);
}
