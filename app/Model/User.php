<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Employee $Employee
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'username';

    var $validate = array(
        'username' => array(
            'username-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a username'
            ),
            'username-unique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must select a different username'
            )
        ),
        'password' => array(
            'password-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a user password'
            )
        ),
        'email' => array(
            'email-different' => array(
                'rule' => 'isUnique',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a unique email address.'
            ),
            'email-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter an email address.'
            )
        ),
        'employee_id' => array(
            'employee-id-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must select an employee'
            ),
            'employee-id-unique' => array(
                'rule' => 'isUnique',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a different employee ID.'
            )
        )

    );


    /**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role'
		)
	);
	public function beforeSave($options = array()) {
		// password
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}
 /**
	// strip out user passwords when retrieving
	public function afterFind($results, $primary = false) {
		foreach($results as $key => $value) {
			if(isset($results[$key]['User']['password'])) {
				unset($results[$key]['User']['password']);
			}
		}
		return $results;
	}

  **/
}