<?php
App::uses('AppModel', 'Model');
/**
 * Coordinate Model
 *
 * @property Broker $Broker
 * @property Customer $Customer
 */
class Coordinate extends AppModel {


    var $validate = array(
        'latitude' => array(
            'lat-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a latitude'
            ),
            'lat-decimal' => array(
                'rule' => array('decimal', 6),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter an accurate latitude.'
            )
        ),
        'longitude' => array(
            'long-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a longitude'
            ),
            'long-number' => array(
                'rule' => array('decimal', 6),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter an accurate longitude.'
            )
        )
    );


/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Broker' => array(
			'className' => 'Broker',
			'foreignKey' => 'coordinate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'coordinate_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
