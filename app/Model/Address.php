<?php
App::uses('AppModel', 'Model');
/**
 * Address Model
 *
 * @property Person $Person
 */
class Address extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';




/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'address_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
