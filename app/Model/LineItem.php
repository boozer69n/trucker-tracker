<?php
App::uses('AppModel', 'Model');
/**
 * LineItem Model
 *
 * @property Invoice $Invoice
 */
class LineItem extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';


    var $validate = array(
        'description' => array(
            'desc-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a description'
            )
        ),
        'price' => array(
            'price-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must have some price at least 0.00'
            ),
            'price-decimal' => array(
                'rule' => array('money', 'left'),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Must be a currency format.'
            )
        ),
        'invoice_id' => array(
            'id-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false
            )
        )
    );


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Invoice' => array(
			'className' => 'Invoice',
			'foreignKey' => 'invoice_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
