<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property Person $Person
 * @property Coordinate $Coordinate
 * @property Invoice $Invoice
 */
class Customer extends AppModel {

	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['name'] = $this->Person->virtualFields['name'];
	}

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';




/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Coordinate' => array(
			'className' => 'Coordinate',
			'foreignKey' => 'coordinate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Invoice' => array(
			'className' => 'Invoice',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
