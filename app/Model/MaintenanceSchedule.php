<?php
App::uses('AppModel', 'Model');
/**
 * MaintenanceSchedule Model
 *
 * @property MaintenanceType $MaintenanceType
 * @property Equipment $Equipment
 */
class MaintenanceSchedule extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';




/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'MaintenanceType' => array(
			'className' => 'MaintenanceType',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Equipment' => array(
			'className' => 'Equipment',
			'foreignKey' => 'equipment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
