<?php
App::uses('AppModel', 'Model');
/**
 * Stop Model
 *
 * @property Trip $Trip
 * @property Commodity $Commodity
 */
class Stop extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';




/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Trip' => array(
			'className' => 'Trip',
			'foreignKey' => 'trip_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Commodity' => array(
			'className' => 'Commodity',
			'foreignKey' => 'stop_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
