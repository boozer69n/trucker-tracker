<?php
App::uses('AppModel', 'Model');
/**
 * Commodity Model
 *
 * @property Stop $Stop
 */
class Commodity extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	//public $useTable = 'commodity';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';




/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Stop' => array(
			'className' => 'Stop',
			'foreignKey' => 'stop_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
