<?php
App::uses('AppModel', 'Model');
/**
 * Maintenance Model
 *
 * @property MaintenanceType $MaintenanceType
 * @property MaintenanceSchedule $MaintenanceSchedule
 */
class Maintenance extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'description';




/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MaintenanceType' => array(
			'className' => 'MaintenanceType',
			'foreignKey' => 'type',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
