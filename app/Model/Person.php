<?php
App::uses('AppModel', 'Model');
/**
 * Person Model
 *
 * @property Address $Address
 * @property Notes $Notes
 */
class Person extends AppModel {

	public $virtualFields = array('name' => 'CONCAT(first_name, " ", last_name)');
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'last_name';


    var $validate = array(
        'first_name' => array(
            'not-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a first name.'
            )
        ),
        'last_name' => array(
            'not-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a last name'
            )
        ),
        'zip' => array(
            'numeric-zip' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a zip'
            ),
            'zip-5-digits' => array(
                'rule' => array('postal', null, 'us'),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a zipcode'
            )
        ),
        'street' => array(
            'street-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a street'
            )
        ),
        'city' => array(
            'city-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a city'
            )
        ),
        'state' => array(
            'state-null' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a state'
            )
        )
    );


/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Notes' => array(
			'className' => 'Notes',
			'foreignKey' => 'creator',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
