<?php
App::uses('AppModel', 'Model');
/**
 * Broker Model
 *
 * @property Person $Person
 * @property Coordinate $Coordinate
 */
class Broker extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'business_name';

    var $validate = array(
        'coordinate_id' => array(
            'coord-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must have correct coordinates selected'
            )
        ),
        'business_name' => array(
            'business-null' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'You must enter a business name'
            )
        )
    );

	

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Person' => array(
			'className' => 'Person',
			'foreignKey' => 'id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Coordinate' => array(
			'className' => 'Coordinate',
			'foreignKey' => 'coordinate_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
