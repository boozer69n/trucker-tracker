<?php
/**
 * Author: Blake
 * Date: 2/7/2015
 * Time: 4:56 PM
 * File: DashboardController.php
 */


App::uses('CakeNumber', 'Utility');

class DashboardController extends AppController {
    public function index() {
        // load the maintenance model
        $this->loadModel('Maintenance');
        // get the last 15 maintenances
        $this->set('maintenances', $this->Maintenance->find('all', array('limit' => 15, 'recursive' => 2)));


        // load the invoices
        $this->loadModel('Invoice');
        $income = $this->Invoice->query("
SELECT SUM(`Item`.`price`) as money
FROM  `line_items` AS  `Item` LEFT JOIN
 (Select `invoices`.`created`, `invoices`.`id` from `invoices`
  WHERE YEAR(FROM_UNIXTIME(`invoices`.`created`)) = YEAR(CURDATE())) AS  `Invoice`
ON  `Item`.`invoice_id` =  `Invoice`.`id`");
        $this->set('income', $income[0][0]['money']);
        $this->set('last_total', $this->Invoice->find('first', array('order' => array('Invoice.created' => 'DESC'))));

        // customer counter
        $this->loadModel('Customer');
        $this->set('customer_count', $this->Customer->find('count'));
    }
}