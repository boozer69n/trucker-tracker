<?php
App::uses('AppController', 'Controller');
/**
 * Employees Controller
 *
 * @property Employee $Employee
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EmployeesController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'Session');

	public function isAuthorized($user) {
		// type can be a small int with a small table to quantify names.+
		if($this->Auth->user('role') == 1) {
			return true; // return true if they are an admin
		} else {
			return false; // otherwise lets just return false for no access
		}
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Employee->recursive = 0;
		$this->set('employees', $this->Paginator->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
		$this->set('employee', $this->Employee->find('first', $options));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$this->loadModel('Person');            // load the Person Model for use too (This has address and everything in it)
		if ($this->request->is('post')) {      // check if the form was submitted.
			$this->Employee->create();         // make a new employee
			$data = $this->request->data;      // get the posted data

			/**
			 * Ok this is complicated looking but rather simple.
			 *
			 * First save the Persons Address. If this succeeds link the address to the person by setting the
			 * correct id (address_id in person). Next we save the person itself. If we are still successful the we
			 * save the license.
			 */
			if ($this->Employee->Person->save($data)) {
				$this->Employee->set('id', $this->Employee->Person->id);
				if ($this->Employee->save($this->request->data)) {
					$this->Session->setFlash(__('The employee has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
				}
			} else {
				$this->Session->setFlash(__('The employee could not be saved due to an issue with the Person model. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;
            $this->Employee->Person->set('id', $data['Person']['id']);
            $this->Employee->set('id', $data['Employee']['id']);
            if($this->Employee->Person->save($data)) {
                if ($this->Employee->save($data)) {
                    $this->Session->setFlash(__('The employee has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
                }
            }else {
                $this->Session->setFlash(__('The Person could not be saved. Please, try again.'));
            }
		} else {
			$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
			$this->request->data = $this->Employee->find('first', $options);
		}
	}

	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->Employee->id = $id;
		if (!$this->Employee->exists()) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Employee->delete()) {
			$this->Session->setFlash(__('The employee has been deleted.'));
		} else {
			$this->Session->setFlash(__('The employee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
