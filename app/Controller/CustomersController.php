<?php
App::uses('AppController', 'Controller');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CustomersController extends AppController{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->Customer->recursive = 0;
        $this->set('customers', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null)
    {
        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $this->set('customer', $this->Customer->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        $this->loadModel('Person');            // load the Person Model for use too (This has address and everything in it)
        if ($this->request->is('post')) {      // check if the form was submitted.
            $this->Customer->create();         // make a new customer
            $data = $this->request->data;      // get the posted data

            /**
             * Ok this is complicated looking but rather simple.
             *
             * First save the Persons Address. If this succeeds link the address to the person by setting the
             * correct id (address_id in person). Next we save the person itself. If we are still successful the we
             * save the coordinate. If this was a success we take the new coordinate ID and set the Customers Coordinate
             * to match (coordinate_id).
             */
            if ($this->Customer->Person->save($data)) {
                    if ($this->Customer->Coordinate->save($data)) {
                        $this->Customer->set('id', $this->Customer->Person->id);
                        $this->Customer->set('coordinate_id', $this->Customer->Coordinate->id);
                        if ($this->Customer->save($data)) {
                            $this->Session->setFlash(__('The customer has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                        } else {
                            $this->Session->setFlash(__('The customer could not be saved. Please, try again.'));
                        }
                    } else {
                        $this->Session->setFlash(__('The customer could not be saved due to a coordinate issue. Please, try again.'));
                    }
                } else {
                    $this->Session->setFlash(__('The customer could not be saved due to an issue with the Person model. Please, try again.'));
                }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;
            $this->Customer->Person->set('id', $data['Person']['id']);
            $this->Customer->set('id', $data['Customer']['id']);
            if($this->Customer->Person->save($data)) {
                if ($this->Customer->save($data)) {
                    $this->Session->setFlash(__('The customer has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The customer could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The Person could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
            $this->request->data = $this->Customer->find('first', $options);
        }
        $coordinates = $this->Customer->Coordinate->find('list');
        $this->set(compact('coordinates'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null)
    {
        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Customer->delete()) {
            $this->Session->setFlash(__('The customer has been deleted.'));
        } else {
            $this->Session->setFlash(__('The customer could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
