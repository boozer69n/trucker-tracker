<?php
/**
 * Author: Blake
 * Date: 2/4/2015
 * Time: 6:22 PM
 * File: ApiController.php
 */

class ApiController extends AppController {
    public $helpers = array('Html', 'Form', 'Session');
    public $components = array('Session', 'RequestHandler');

    public function isAuthorized($user) {
        return parent::isAuthorized($user);
    }

    // allow JSON without login.
    public function beforeFilter() {
        parent::beforeFilter();
        // this will allow all to everyone
        //$this->Auth->allow(); // we will need this later!
        // send the header to allow cross domain JSON Pull
        header("Access-Control-Allow-Origin: *");
    }
	
	// get the address list
    public function getAddresses($secret, $key) {
        $this->layout = null;
        $this->loadModel('Address');

        // Return a list of all addresses
        $this->set('addresses', $this->Address->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'addresses');
    }
	
	// get the brokers list
    public function getBrokers($secret, $key) {
        $this->layout = null;
        $this->loadModel('Broker');

        // Return a list of all brokers
        $this->set('brokers', $this->Broker->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'brokers');
    }
	
    // get the customers list
    public function getCustomers($secret, $key) {
        $this->layout = null;
        $this->loadModel('Customer');

        // Return a list of all customers
        $this->set('customers', $this->Customer->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'customers');
    }
	
	// get the employees list
    public function getEmployees($secret, $key) {
        $this->layout = null;
        $this->loadModel('Employee');

        // Return a list of all employees
        $this->set('employees', $this->Employee->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'employees');
    }
	
	// get the equipment list
    public function getEquipment($secret, $key) {
        $this->layout = null;
        $this->loadModel('Equipment');

        // Return a list of all equipment
        $this->set('equipment', $this->Equipment->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'equipment');
    }
	
	// get the invoice list
    public function getInvoices($secret, $key) {
        $this->layout = null;
        $this->loadModel('Invoice');

        // Return a list of all invoices
        $this->set('invoices', $this->Invoice->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'invoices');
    }
	
	// get the line items list
    public function getLineItems($secret, $key) {
        $this->layout = null;
        $this->loadModel('LineItem');

        // Return a list of all line items
        $this->set('lineitems', $this->LineItem->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'lineitems');
    }
	
	// get the notes list
    public function getNotes($secret, $key) {
        $this->layout = null;
        $this->loadModel('Note');

        // Return a list of all notes
        $this->set('notes', $this->Note->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'notes');
    }
	
	// get the people list
    public function getPeople($secret, $key) {
        $this->layout = null;
        $this->loadModel('Person');

        // Return a list of all people
        $this->set('people', $this->Person->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'people');
    }

    // get the users list
    public function getMaintenances($secret, $key) {
        $this->layout = null;
        $this->loadModel('Maintenance');

        // Return a list of all users
        $this->set('maintenances', $this->Maintenance->find('all', $this->buildQuery()));
        // set articles
        $this->set('_serialize', 'maintenances');
    }
	
	
    /**
     * Dynamically build a query based on the arguments passed in.
     */
    private function buildQuery() {
        $conditions = array();
        if(isset($this->params['url']['limit'])) {
            $conditions['limit'] = $this->params['url']['limit'];
        }
        if(isset($this->params['url']['order'])) {
            $conditions['order'] = $this->params['url']['order'];
        }

        return $conditions;
    }
}