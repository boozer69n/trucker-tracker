<?php
App::uses('AppController', 'Controller');
/**
 * LineItems Controller
 *
 * @property LineItem $LineItem
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LineItemsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->LineItem->recursive = 0;
		$this->set('lineItems', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->LineItem->exists($id)) {
			throw new NotFoundException(__('Invalid line item'));
		}
		$options = array('conditions' => array('LineItem.' . $this->LineItem->primaryKey => $id));
		$this->set('lineItem', $this->LineItem->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->LineItem->create();
			if ($this->LineItem->save($this->request->data)) {
				$this->Session->setFlash(__('The line item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The line item could not be saved. Please, try again.'));
			}
		}
		$invoices = $this->LineItem->Invoice->find('list');
		$this->set(compact('invoices'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->LineItem->exists($id)) {
			throw new NotFoundException(__('Invalid line item'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LineItem->save($this->request->data)) {
				$this->Session->setFlash(__('The line item has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The line item could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LineItem.' . $this->LineItem->primaryKey => $id));
			$this->request->data = $this->LineItem->find('first', $options);
		}
		$invoices = $this->LineItem->Invoice->find('list');
		$this->set(compact('invoices'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->LineItem->id = $id;
		if (!$this->LineItem->exists()) {
			throw new NotFoundException(__('Invalid line item'));
		}
		$this->LineItem->read();
		$invoice = $this->LineItem->data['LineItem']['invoice_id'];
		$this->request->allowMethod('post', 'delete');
		if ($this->LineItem->delete()) {
			$this->Session->setFlash(__('The line item has been deleted.'));
		} else {
			$this->Session->setFlash(__('The line item could not be deleted. Please, try again.'));
		}
		$this->redirect(array('controller' => 'Invoices', 'action' => 'view', $invoice));
	}
}
