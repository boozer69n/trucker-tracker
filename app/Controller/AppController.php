<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	protected $user_is_admin;
	public $components = array('Security',
		'Session',
		'Auth' => array(
			'loginRedirect' => array(
				'controller' => 'dashboard',
				'action' => 'index'
			),
			'logoutRedirect' => array(
				'controller' => 'users',
				'action' => 'login'
			),
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
			),
			'authorize' => array('Controller'),
			'unauthorizedRedirect' => array('controller'=>'users', 'action' => 'login') // default to using the index page for unauth
		)
	);

	/**
	 * @return bool True if the user can access an area.
	 *
	 * This is the global allow/deny for all users to pass through
	 */
	public function isAuthorized($user) {
		// type can be a small int with a small table to quantify names.+
		if($this->Auth->user()) {
			return true; // return true if they are an admin
		} else {
			return false; // otherwise lets just return false for no access
		}
	}

	public function beforeFilter() {
		$this->user_is_admin = $this->Auth->user('Role.name') === 'Admin' ? true : false;
		$this->set('is_user',$this->Auth->user());
		$this->set('is_admin', $this->user_is_admin);
		$this->set('username', $this->Auth->User('username'));
		$this->set('referer', $this->referer());
	}

	public function beforeRender() {
		if($this->request->is('ajax')){
			$this->layout = null;
			$this->set('modal-view', true);
		} else {
			$this->layout = 'dashboard';
		}
	}
}
