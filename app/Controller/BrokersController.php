<?php
App::uses('AppController', 'Controller');
/**
 * Brokers Controller
 *
 * @property Broker $Broker
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BrokersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Broker->recursive = 0;
		$this->set('brokers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Broker->exists($id)) {
			throw new NotFoundException(__('Invalid broker'));
		}
		$options = array('conditions' => array('Broker.' . $this->Broker->primaryKey => $id));
		$this->set('broker', $this->Broker->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Person');            // load the Person Model for use too (This has address and everything in it)
        if ($this->request->is('post')) {      // check if the form was submitted.
            $this->Broker->create();         // make a new broker
            $data = $this->request->data;      // get the posted data

            /**
             * Ok this is complicated looking but rather simple.
             *
             * First save the Persons Address. If this succeeds link the address to the person by setting the
             * correct id (address_id in person). Next we save the person itself. If we are still successful the we
             * save the coordinate. If this was a success we take the new coordinate ID and set the Customers Coordinate
             * to match (coordinate_id).
             */
            // get the business to set it on person
            $this->Broker->Person->set('business',  $data['Broker']['business_name']);
            if ($this->Broker->Person->save($data)) {
                    if ($this->Broker->Coordinate->save($data)) {
                        $this->Broker->set('id', $this->Broker->Person->id);
                        $this->Broker->set('coordinate_id', $this->Broker->Coordinate->id);
                        if ($this->Broker->save($data)) {
                            $this->Session->setFlash(__('The broker has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                        } else {
                            $this->Session->setFlash(__('The broker could not be saved. Please, try again.'));
                        }
                    } else {
                        $this->Session->setFlash(__('The broker could not be saved due to a coordinate issue. Please, try again.'));
                    }
                } 
			else {
                    $this->Session->setFlash(__('The broker could not be saved due to an issue with the Person model. Please, try again.'));
            }
        }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Broker->exists($id)) {
			throw new NotFoundException(__('Invalid broker'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;
            $this->Broker->Person->set('business',  $data['Broker']['business_name']);
            $this->Broker->Person->set('id', $data['Person']['id']);
            $this->Broker->set('id', $data['Broker']['id']);
            if($this->Broker->Person->save($data)) {
                if ($this->Broker->save($this->request->data)) {
                    $this->Session->setFlash(__('The broker has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The broker could not be saved. Please, try again.'));
                }
            } else {
                $this->Session->setFlash(__('The Person could not be saved. Please, try again.'));
            }
		} else {
			$options = array('conditions' => array('Broker.' . $this->Broker->primaryKey => $id));
			$this->request->data = $this->Broker->find('first', $options);
		}
		$coordinates = $this->Broker->Coordinate->find('list');
		$this->set(compact('coordinates'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Broker->id = $id;
		if (!$this->Broker->exists()) {
			throw new NotFoundException(__('Invalid broker'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Broker->delete()) {
			$this->Session->setFlash(__('The broker has been deleted.'));
		} else {
			$this->Session->setFlash(__('The broker could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
