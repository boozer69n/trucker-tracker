<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function beforeFilter(){
		parent::beforeFilter();
		// allow anyone to see the following actions.
		$this->Auth->allow(
			'login',
			'logout');
	}

	public function isAuthorized($user) {
		// type can be a small int with a small table to quantify names.+
		if($this->Auth->user('role') == 1) {
			return true; // return true if they are an admin
		} else {
			return false; // otherwise lets just return false for no access
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function register() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('You have successfully registered the user.'), 'default', array('class'=>'success'), 'flash');
				return $this->redirect(array('controller'=>'dashboard', 'action' => 'index'));
			}
			$this->Session->setFlash(
				__('The user could not be registered. Please, try again.')
			);
		}

		$roles = $this->User->Role->find('list');
		// This nutcase of a query only retrieves the employees that ARE NOT already a user
		$employees = $this->User->Employee->find('list', array('fields' => array('Employee.id', 'Employee.name'),
			'conditions' => array('not exists (select employee_id from users where users.employee_id = Employee.id)'),
			'recursive' => 1));
		$this->set(compact('employees', 'roles'));
	}

	// log a user in and redirect them to the main page
	public function login() {

		// check to see if the post is made or
		// check to see if the user is logged in
		if ($this->request->is('post') ||
			(bool)$this->Auth->user()) {
			if ($this->Auth->login()) {
                $this->User->id = $this->Auth->user('id');
                $this->User->saveField('id', $this->Auth->user('id'));
				// Role Redirect
				return $this->redirect($this->Auth->redirect(array('controller'=>'dashboard', 'action' => 'index')));
			}
			$this->Session->setFlash(__('Invalid username or password, try again'));
		}
	}

	// log a user out.
	public function logout() {
		$this->Session->setFlash(__('You have been successfully logged out.'), 'default', array('class'=>'success'), 'flash');
		return $this->redirect($this->Auth->logout());
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
