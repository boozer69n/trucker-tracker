<div class="commodities view">
<h2><?php echo __('Commodity'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Commodity ID'); ?></th>
				<th><?php echo __('Stop Description'); ?></th>
				<th><?php echo __('Alteration'); ?></th>
				<th><?php echo __('Commodity Type'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($commodity['Commodity']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($commodity['Stop']['description'], array('controller' => 'stops', 'action' => 'view', $commodity['Stop']['id'])); ?>
				</td>
				<td><?php echo h($commodity['Commodity']['alteration']); ?></td>
				<td><?php echo h($commodity['Commodity']['commodity_type']); ?></td>
			</tr>
		</table>
	</div>
</div>

