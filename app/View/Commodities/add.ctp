<div class="commodities form">
<?php echo $this->Form->create('Commodity'); ?>
	<fieldset>
		<legend><?php echo __('Add Commodity'); ?></legend>
	<?php
		echo $this->Form->input('stop_id', array('class' => 'btn btn-default'));
		echo $this->Form->input('alteration');
		echo $this->Form->input('commodity_type');
	?>
	</fieldset>
<div class='buttons'>
<?php
	echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary'));
	echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
?>
</div>
<?php echo $this->Form->end(); ?>
</div>
