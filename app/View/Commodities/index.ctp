<div class="commodities index">
	<h2><?php echo __('Commodities'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('stop_id'); ?></th>
			<th><?php echo $this->Paginator->sort('alteration'); ?></th>
			<th><?php echo $this->Paginator->sort('commodity_type'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($commodities as $commodity): ?>
	<tr>
		<td><?php echo h($commodity['Commodity']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($commodity['Stop']['description'], array('controller' => 'stops', 'action' => 'view', $commodity['Stop']['id'])); ?>
		</td>
		<td><?php echo h($commodity['Commodity']['alteration']); ?>&nbsp;</td>
		<td><?php echo h($commodity['Commodity']['commodity_type']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $commodity['Commodity']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $commodity['Commodity']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $commodity['Commodity']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $commodity['Commodity']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
