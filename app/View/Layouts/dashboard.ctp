<!-- Head -->
<?php echo $this->element('head'); ?>

<body>
<div class="wrapper">
    <!-- Charset -->
    <?php echo $this->Html->charset(); ?>

    <!-- Header and Nav menu -->
    <?php echo $this->element('navigation'); ?>

    <!-- Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <!-- Side Navigation -->
                <?php
                echo $this->element('side_navigation');
                if($is_admin){
                   echo $this->element('admin_side_nav');
                }
                ?>

            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <!-- Flash -->
                <?php echo $this->element('flash'); ?>
                <!-- Main Content Area -->
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php echo $this->element('footer'); ?>
</body>
</html>