<div class="employees index">
	<h2><?php echo __('Employees'); ?></h2>
	
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Person.name', 'Employee'); ?></th>
			<th><?php echo $this->Paginator->sort('license'); ?></th>
			<th><?php echo $this->Paginator->sort('Person.cell_number', 'Phone'); ?></th>
			<th><?php echo $this->Paginator->sort('created', 'Since'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($employees as $employee): ?>
	<tr>
		<td><?php echo h($employee['Employee']['id']); ?>&nbsp;</td>
		<td><?php echo h($employee['Person']['name']); ?>&nbsp;</td>
		<td><?php echo h($employee['Employee']['license']); ?>&nbsp;</td>
		<td><?php echo h($employee['Person']['cell_number']); ?>&nbsp;</td>
		<td><?php echo $this->Time->format($employee['Employee']['created'], '%B %e, %Y'); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $employee['Employee']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $employee['Employee']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $employee['Employee']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $employee['Employee']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<br/>
<?php echo $this->Html->link(__('New Employee'), array('action' => 'add'), array('class' => 'btn btn-default')); ?>