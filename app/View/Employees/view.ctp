<?php
	$a = (($employee['Person']['street'])."   ");
	$b = (($employee['Person']['city']).', ');
	$c = ($employee['Person']['state']);
	$address = $a;
	$city = $b.$c;
	?>

<div class="employees view">
<h2><?php echo __('Employee'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Employee ID'); ?></th>
				<th><?php echo __('Employee Name'); ?></th>
				<th><?php echo __('License'); ?></th>
				<th><?php echo __('Phone Number'); ?></th>
				<th><?php echo __('Address'); ?></th>
				<th><?php echo __('City'); ?></th>
				<th><?php echo __('Employee Since'); ?></th>
			</tr>
			<tr>
				<td><?php echo ($employee['Employee']['id']); ?></td>
				<td><?php echo ($employee['Employee']['name']); ?></td>
				<td><?php echo ($employee['Employee']['license']); ?></td>
				<td><?php echo ($employee['Person']['cell_number']); ?></td>
				<td><?php echo ($address); ?> </td>
				<td><?php echo ($city); ?> </td>
				<td><?php echo $this->Time->format($employee['Employee']['created'], '%B %e, %Y'); ?></td>
			</tr>
		</table>
	</div>
</div>


<div class="related">
	<h3><?php echo __('Related Equipment'); ?></h3>
	<?php if (!empty($employee['Equipment'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Plate'); ?></th>
		<th><?php echo __('Employee Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($employee['Equipment'] as $equipment): ?>
		<tr>
			<td><?php echo $equipment['id']; ?></td>
			<td><?php echo $equipment['make']; ?></td>
			<td><?php echo $equipment['model']; ?></td>
			<td><?php echo $equipment['year']; ?></td>
			<td><?php echo $equipment['plate']; ?></td>
			<td><?php echo $equipment['employee_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'equipment', 'action' => 'view', $equipment['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'equipment', 'action' => 'edit', $equipment['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'equipment', 'action' => 'delete', $equipment['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $equipment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<?php echo $this->Html->link(__('New Equipment'), array('controller' => 'equipment', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
	</div>
</div>