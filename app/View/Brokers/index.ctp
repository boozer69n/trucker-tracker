<div class="brokers index">
	<h2><?php echo __('Brokers'); ?></h2>
	
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('ID'); ?></th>
			<th><?php echo $this->Paginator->sort('business_name'); ?></th>
			<th><?php echo $this->Paginator->sort('Broker Since'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($brokers as $broker): ?>
	<tr>
		<td><?php echo h($broker['Broker']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($broker['Broker']['business_name'], array('controller' => 'brokers', 'action' => 'view', $broker['Broker']['id'])); ?>&nbsp;
		</td>		
		<td><?php echo $this->Time->format($broker['Broker']['created'], '%B %e, %Y'); ?>&nbsp;</td>		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $broker['Broker']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $broker['Broker']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $broker['Broker']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $broker['Broker']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<br />
<?php echo $this->Html->link(__('New Broker'), array('action' => 'add'), array('class' => 'btn btn-default')); ?>