<?php
	if(($broker['Person']['cell_number']) == null)
	{
		$number = ($broker['Person']['home_number']);
	}else{
		$number =($broker['Person']['cell_number']);
	}
?>

<div class="brokers view">
<h2><?php echo __('Broker'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Broker ID'); ?></th>
				<th><?php echo __('Business Name'); ?></th>
				<th><?php echo __('Contact Name'); ?></th>
				<th><?php echo __('Phone Number'); ?></th>
				<th><?php echo __('Customer Since'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($broker['Broker']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($broker['Broker']['business_name'], array('controller' => 'brokers', 'action' => 'view', $broker['Broker']['id'])); ?>
				</td>
				<td><?php echo h($broker['Person']['name']); ?></td>
				<td><?php echo h($number); ?></td>
				<td><?php echo $this->Time->format($broker['Broker']['created'], '%B %e, %Y'); ?></td>
			</tr>
		</table>
	</div>	
</div>
<?php echo $this->Html->link(__('Edit Broker'), array('controller' => 'brokers', 'action' => 'edit', $broker['Broker']['id']), array('class' => 'btn btn-default')); ?>
