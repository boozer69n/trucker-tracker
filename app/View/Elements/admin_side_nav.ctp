<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Employees", array('controller' => 'employees', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- Add Employee", array('controller' => 'employees', 'action' => 'add')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Users", array('controller' => 'users', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- Add User", array('controller' => 'users', 'action' => 'register')); ?></li>
</ul>