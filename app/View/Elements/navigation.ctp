<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='<?php echo Router::url('/'); ?>'><span class="glyphicon glyphicon-road" aria-hidden="true"></span> Trucker Tracker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><? echo $this->Html->link("Dashboard", array('controller' => 'dashboard', 'action' => 'index')); ?></li>
                <li class="dropdown extra-mobile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Trips<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><? echo $this->Html->link("Trips", array('controller' => 'trips', 'action' => 'index')); ?></li>
                        <li><? echo $this->Html->link("- New Trip", array('controller' => 'trips', 'action' => 'add')); ?></li>
                    </ul>
                </li>
                <li class="dropdown extra-mobile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Invoices<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><? echo $this->Html->link("Invoices", array('controller' => 'invoices', 'action' => 'index')); ?></li>
                        <li><? echo $this->Html->link("- New Invoice", array('controller' => 'invoices', 'action' => 'add')); ?></li>
                    </ul>
                </li>
                    <? if($is_user) {
                        echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>$username<span class='caret'></span></a>";
                        echo "<ul class='dropdown-menu' role='menu'><li>";
                        echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));
                        echo "</li></ul>";
                    } else {
                        echo "<li>";
                        echo $this->Html->link('Sign In', array('controller' => 'users', 'action' => 'login'));
                    } ?>
                </li>
            </ul>
        </div>
    </div>
</nav>