<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Dashboard", array('controller' => 'dashboard', 'action' => 'index')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Trips", array('controller' => 'trips', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- New Trip", array('controller' => 'trips', 'action' => 'add')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Invoices", array('controller' => 'invoices', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- New Invoice", array('controller' => 'invoices', 'action' => 'add')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Customers", array('controller' => 'customers', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- Add Customer", array('controller' => 'customers', 'action' => 'add')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Brokers", array('controller' => 'brokers', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- Add Broker", array('controller' => 'brokers', 'action' => 'add')); ?></li>
</ul>
<ul class="nav nav-sidebar">
    <li><? echo $this->Html->link("Equipment", array('controller' => 'equipment', 'action' => 'index')); ?></li>
    <li><? echo $this->Html->link("- Add Equipment", array('controller' => 'equipment', 'action' => 'add')); ?></li>
</ul>