<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
    <meta charset="utf-8">
    <!-- If you delete this meta tag World War Z will become a reality -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A site for csc453 class UAT">
    <meta name="author" content="Team A - CSC453">

    <!-- Keywords -->
    <?php
    if (!empty($meta_keywords)) {
        echo $this->Html->meta(
            'keywords',
            $meta_keywords);
    }
    ?>

    <!-- Description -->
    <?php
    if (!empty($meta_description)) {
        echo $this->Html->meta(
            'keywords',
            $meta_description);
    }
    ?>

    <!-- Title -->
    <title><?php echo $this->fetch('title'); ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- External Files -->
    <?php
    // CSS
    echo $this->Html->css(
        array(
            'normalize',            // normalizer
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',        // bootstrap
            'main'                  // custom
        )
    );


    // load these last to increase content loading speeds
    // JavaScript
    echo $this->Html->script(
        array(
            'jquery.min',
            'bootstrap.min',
        )
    );

    // Fetch
    echo $this->fetch('css');
    echo $this->fetch('meta');
    echo $this->fetch('script');
    ?>

    <!-- Google Analytics -->
    <!-- if we had one -->
</head>