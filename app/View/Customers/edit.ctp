<div class="customers form">
<?php echo $this->Form->create('Customer'); ?>
	<fieldset>
		<legend><?php echo __('Edit Customer'); ?></legend>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('Person.id');
        echo $this->Form->input('Person.first_name');
        echo $this->Form->input('Person.last_name');
        echo $this->Form->input('Person.home_number');
        echo $this->Form->input('Person.cell_number');
        echo $this->Form->input('Person.email');
        echo $this->Form->input('Person.street');
        echo $this->Form->input('Person.street_line2');
        echo $this->Form->input('Person.city');
        echo $this->Form->input('Person.state');
        echo $this->Form->input('Person.zip');
	?>
	</fieldset>
<br/>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

