<?php
	$a = (($customer['Person']['street'])."   ");
	$b = (($customer['Person']['city']).', ');
	$c = ($customer['Person']['state']);
	$address = $a;
	$city = $b.$c;
	
	if(($customer['Person']['cell_number']) == null)
		{
			$phone = ($customer['Person']['home_number']);
		}else{
			$phone =($customer['Person']['cell_number']);
		}
?>

<div class="customers view">
<h2><?php echo __('Customer'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Customer ID'); ?></th>
				<th><?php echo __('Customer Name'); ?></th>
				<th><?php echo __('Address'); ?></th>
				<th><?php echo __('City'); ?></th>
				<th><?php echo __('Phone Number'); ?></th>
				<th><?php echo __('Customer Since'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($customer['Customer']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($customer['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $customer['Customer']['id'])); ?>
				</td>
				<td><?php echo h($address); ?></td>
				<td><?php echo h($city); ?></td>
				<td><?php echo h($phone); ?></td>
				<td><?php echo $this->Time->format($customer['Customer']['created'], '%B %e, %Y'); ?></td>
			</tr>
		</table>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related Invoices'); ?></h3>
	<?php if (!empty($customer['Invoice'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($customer['Invoice'] as $invoice): ?>
		<tr>
			<td><?php echo $invoice['id']; ?></td>
			<td><?php echo $invoice['customer_id']; ?></td>
			<td><?php echo $this->Time->format($invoice['created'], '%B %e, %Y'); ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'invoices', 'action' => 'view', $invoice['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'invoices', 'action' => 'edit', $invoice['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'invoices', 'action' => 'delete', $invoice['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $invoice['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
	
			<?php echo $this->Html->link(__('New Invoice'), array('controller' => 'invoices', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
		
	</div>
</div>
