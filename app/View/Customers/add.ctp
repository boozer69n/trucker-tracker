<div class="customers form">
<?php echo $this->Form->create('Customer'); ?>
	<fieldset>
		<legend><?php echo __('Add Customer'); ?></legend>
	<?php
		echo $this->Form->input('Person.first_name');
		echo $this->Form->input('Person.last_name');
		echo $this->Form->input('Person.home_number');
		echo $this->Form->input('Person.cell_number');
		echo $this->Form->input('Person.email');
		echo $this->Form->input('Person.business');
		echo $this->Form->input('Person.street');
		echo $this->Form->input('Person.street_line2');
		echo $this->Form->input('Person.city');
		echo $this->Form->input('Person.state');
		echo $this->Form->input('Person.zip');
		echo $this->Form->input('Coordinate.latitude');
		echo $this->Form->input('Coordinate.longitude');
	?>
	</fieldset>
	<br />
	<div class='buttons'>
	<?php
		echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary'));
		echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
	?>
	</div>
<?php echo $this->Form->end(); ?>
</div>