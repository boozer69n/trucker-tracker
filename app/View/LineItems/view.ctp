<div class="lineItems view">
<h2><?php echo __('Line Item'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Item ID'); ?></th>
				<th><?php echo __('Description'); ?></th>
				<th><?php echo __('Price'); ?></th>
				<th><?php echo __('Associated Invoice ID'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($lineItem['LineItem']['id']); ?></td>
				<td><?php echo h($lineItem['LineItem']['description']); ?></td>
				<td><?php echo h($lineItem['LineItem']['price']); ?></td>
				<td>
					<?php echo $this->Html->link($lineItem['Invoice']['id'], array('controller' => 'invoices', 'action' => 'view', $lineItem['Invoice']['id'])); ?>
				</td>
			</tr>
		</table>
	</div>
</div>

