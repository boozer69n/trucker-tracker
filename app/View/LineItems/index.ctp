<div class="lineItems index">
	<h2><?php echo __('Line Items'); ?></h2>
	
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('price'); ?></th>
			<th><?php echo $this->Paginator->sort('invoice_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($lineItems as $lineItem): ?>
	<tr>
		<td><?php echo h($lineItem['LineItem']['id']); ?>&nbsp;</td>
		<td><?php echo h($lineItem['LineItem']['description']); ?>&nbsp;</td>
		<td><?php echo h($lineItem['LineItem']['price']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($lineItem['Invoice']['id'], array('controller' => 'invoices', 'action' => 'view', $lineItem['Invoice']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $lineItem['LineItem']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $lineItem['LineItem']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lineItem['LineItem']['id']), __('Are you sure you want to delete # %s?', $lineItem['LineItem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
