<div class="lineItems form">
<?php echo $this->Form->create('LineItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Line Item'); ?></legend>
	<?php
		echo $this->Form->input('description');
		echo $this->Form->input('price');
		echo $this->Form->input('invoice_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
