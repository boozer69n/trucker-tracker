<!-- File: /app/View/Pages/index.ctp -->

<?php
// Meta Keywords
$this->set(
    'meta_keywords',
    'Home, CSC453, UAT');

// Meta Description
$this->set(
    'meta_description',
    'This is the homepage for CSC453 UAT Trucker Tracker');

// Page Title
$this->set(
    'title_for_layout',
    'Trucker Tracker');

// Page Layout
$this->layout = 'dashboard';
?>

<!-- Home Media -->
<h1 class="page-header">Dashboard</h1>

<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2><? echo $this->Number->currency($income, 'USD');?></h2>
        <span class="text-muted">Income to Date</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2><?php echo $customer_count; ?> </h2>
        <span class="text-muted">Customers to Date</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2><? echo$last_total['Person']['name'];?></h2>
        <span class="text-muted">Last Customer</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2><? echo $this->Number->currency($last_total['Invoice']['total'], 'USD');?></h2>
        <span class="text-muted">Last Invoice</span>
    </div>
</div>

<h2 class="sub-header">Recent Maintenance</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Type</th>
            <th>Description</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($maintenances as $maintenance): ?>
            <tr>
                <td><?php echo h($maintenance['Maintenance']['id']); ?>&nbsp;</td>
                <td><?php echo $this->Time->format($maintenance['Maintenance']['date'], '%B %e, %Y'); ?>&nbsp;</td>
                <td><?php echo h($maintenance['MaintenanceType']['name']); ?>&nbsp;</td>
                <td><?php echo h($maintenance['Maintenance']['description']); ?>&nbsp;</td>
                <td><?php echo $this->Number->currency($maintenance['Maintenance']['expense'], 'USD'); ?>&nbsp;</td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>