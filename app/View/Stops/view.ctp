<div class="stops view">
<h2><?php echo __('Stop'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Stop ID'); ?></th>
				<th><?php echo __('Trip ID'); ?></th>
				<th><?php echo __('Stop Date'); ?></th>
				<th><?php echo __('Stop Description'); ?></th>
				<th><?php echo __('Stop Odometer'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($stop['Stop']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($stop['Trip']['id'], array('controller' => 'trips', 'action' => 'view', $stop['Trip']['id'])); ?>
				</td>
				<td><?php echo $this->Time->format($stop['Stop']['date'], '%B %e, %Y'); ?></td>
				<td><?php echo h($stop['Stop']['description']); ?></td>
				<td><?php echo h($stop['Stop']['odometer']); ?></td>
			</tr>
		</table>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related Commodities'); ?></h3>
	<?php if (!empty($stop['Commodity'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Stop Id'); ?></th>
		<th><?php echo __('Alteration'); ?></th>
		<th><?php echo __('Commodity Type'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($stop['Commodity'] as $commodity): ?>
		<tr>
			<td><?php echo $commodity['id']; ?></td>
			<td><?php echo $commodity['stop_id']; ?></td>
			<td><?php echo $commodity['alteration']; ?></td>
			<td><?php echo $commodity['commodity_type']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'commodities', 'action' => 'view', $commodity['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'commodities', 'action' => 'edit', $commodity['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'commodities', 'action' => 'delete', $commodity['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $commodity['id'])); ?>

			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<?php echo $this->Html->link(__('New Commodity'), array('controller' => 'commodities', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
	</div>
</div>
