<div class="stops form">
<?php echo $this->Form->create('Stop'); ?>
	<fieldset>
		<legend><?php echo __('Edit Stop'); ?></legend>
	<?php
		echo $this->Form->hidden('id');
		echo $this->Form->hidden('trip_id');
		echo $this->Form->input('date', array('class' => 'btn btn-default'));
		echo $this->Form->input('description');
		echo $this->Form->input('odometer');
	?>
	</fieldset>
	<div class='buttons'>
	<?php
		echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary'));
		echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
	?>
	</div>
<?php echo $this->Form->end(); ?>
</div>
