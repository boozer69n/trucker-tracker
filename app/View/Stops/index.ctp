<div class="stops index">
	<h2><?php echo __('Stops'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('trip_id'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('odometer'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($stops as $stop): ?>
	<tr>
		<td><?php echo h($stop['Stop']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($stop['Trip']['id'], array('controller' => 'trips', 'action' => 'view', $stop['Trip']['id'])); ?>
		</td>
		<td><?php echo $this->Time->format($stop['Stop']['date'], '%B %e, %Y'); ?>&nbsp;</td>
		<td><?php echo h($stop['Stop']['description']); ?>&nbsp;</td>
		<td><?php echo h($stop['Stop']['odometer']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $stop['Stop']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $stop['Stop']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $stop['Stop']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $stop['Stop']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>