<div class="row login">
    <div class="">
        <!-- Login Form -->
        <?php echo $this->Form->create('User'); ?>
        <fieldset>
            <legend>
                <?php echo __('Login'); ?>
            </legend>
            <?php
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            ?>
            <br />
            <?php
            echo $this->Form->submit('Login', array('type'=>'submit',  'class' => 'btn btn-primary'));
            echo $this->Form->end();
            ?>
        </fieldset>
    </div>
</div>