<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Register User'); ?></legend>
        <?php
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('email');
        echo $this->Form->input('role', array('class' => 'btn btn-default'));
        echo $this->Form->input('employee_id', array('class' => 'btn btn-default'));
        ?>
    </fieldset>
	<div class='buttons'>
	<?php
		echo $this->Form->submit(__('Register'), array('class' => 'btn btn-primary'));
		echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
	?>
	</div>
<?php echo $this->Form->end(); ?>