<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('User ID'); ?></th>
				<th><?php echo __('Username'); ?></th>
				<th><?php echo __('Password'); ?></th>
				<th><?php echo __('Email'); ?></th>
				<th><?php echo __('Last Sign In'); ?></th>
				<th><?php echo __('Employee'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($user['User']['id']); ?></td>
				<td><?php echo h($user['User']['username']); ?></td>
				<td><?php echo h($user['User']['password']); ?></td>
				<td><?php echo h($user['User']['email']); ?></td>
				<td><?php echo $this->Time->format($user['User']['last_on'], '%B %e, %Y'); ?></td>
				<td>
					<?php echo $this->Html->link($user['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $user['Employee']['id'])); ?>
				</td>
			</tr>
		</table>
	</div>
</div>

