<div class="invoices view">
<h2><?php echo __('Invoice'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Invoice ID'); ?></th>
				<th><?php echo __('Customer'); ?></th>
				<th><?php echo __('Total'); ?></th>
				<th><?php echo __('Created'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($invoice['Invoice']['id']); ?></td>
				<td>
					<?php echo $this->Html->link($invoice['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $invoice['Customer']['id'])); ?>
				</td>
				<td><?php echo $this->Number->currency($invoice['Invoice']['total'], 'USD'); ?></td>
				<td><?php echo $this->Time->format($invoice['Invoice']['created'], '%B %e, %Y'); ?></td>
			</tr>
		</table>
	</div>
	<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalPopOver" data-action="<? echo $this->Html->url(array('controller' => 'invoices', 'action' => 'additem', $invoice['Invoice']['id'])); ?>">Add Item</button>
</div>
<div class="related">
	<h3><?php echo __('Line Items'); ?></h3>
	<?php if (!empty($invoice['LineItem'])): ?>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Invoice Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($invoice['LineItem'] as $lineItem): ?>
		<tr>
			<td><?php echo $lineItem['id']; ?></td>
			<td><?php echo $lineItem['description']; ?></td>
			<td><?php echo $this->Number->currency($lineItem['price']); ?></td>
			<td><?php echo $lineItem['invoice_id']; ?></td>
			<td class="actions">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalPopOver" data-action="<? echo $this->Html->url(array('controller' => 'line_items', 'action' => 'edit', $lineItem['id'])); ?>">Edit</button>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'line_items', 'action' => 'delete', $lineItem['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $lineItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
		</div>
<?php endif; ?>
</div>

<?php echo $this->element('modal'); ?>
<script type="application/javascript">
	$('#modalPopOver').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var action = button.data('action') // Extract info from data-* attributes
// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title').text('Add Item')
		modal.find('.modal-body').load(action)
	})
</script>
