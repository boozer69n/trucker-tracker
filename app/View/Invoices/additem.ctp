<div class="lineItems form">
<?php echo $this->Form->create('LineItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Line Item'); ?></legend>
	<?php
		echo $this->Form->input('description');
		echo $this->Form->input('price');
	?>
	</fieldset>
	<div class='buttons'>
	<?php
		echo $this->Form->submit(__('Add Item'), array('class' => 'btn btn-primary'));
		echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
	?>
	</div>
<?php echo $this->Form->end(); ?>
</div>