<div class="equipment view">
<h2><?php echo __('Equipment'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Equipment ID'); ?></th>
				<th><?php echo __('Make'); ?></th>
				<th><?php echo __('Model'); ?></th>
				<th><?php echo __('Year'); ?></th>
				<th><?php echo __('Plate'); ?></th>
				<th><?php echo __('Associated Employee'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($equipment['Equipment']['id']); ?></td>
				<td><?php echo h($equipment['Equipment']['make']); ?></td>
				<td><?php echo h($equipment['Equipment']['model']); ?></td>
				<td><?php echo h($equipment['Equipment']['year']); ?></td>
				<td><?php echo h($equipment['Equipment']['plate']); ?></td>
				<td>
					<?php echo $this->Html->link($equipment['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $equipment['Employee']['id'])); ?>
				</td>
			</tr>
		</table>
	</div>
</div>
