<div class="equipment index">
	<h2><?php echo __('Equipment'); ?></h2>
	
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('make'); ?></th>
			<th><?php echo $this->Paginator->sort('model'); ?></th>
			<th><?php echo $this->Paginator->sort('year'); ?></th>
			<th><?php echo $this->Paginator->sort('plate'); ?></th>
			<th><?php echo $this->Paginator->sort('Person.name', 'Employee'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($equipment as $equipment): ?>
	<tr>
		<td><?php echo h($equipment['Equipment']['id']); ?>&nbsp;</td>
		<td><?php echo h($equipment['Equipment']['make']); ?>&nbsp;</td>
		<td><?php echo h($equipment['Equipment']['model']); ?>&nbsp;</td>
		<td><?php echo h($equipment['Equipment']['year']); ?>&nbsp;</td>
		<td><?php echo h($equipment['Equipment']['plate']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($equipment['Person']['name'], array('controller' => 'employees', 'action' => 'view', $equipment['Employee']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $equipment['Equipment']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $equipment['Equipment']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $equipment['Equipment']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $equipment['Equipment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
		</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<br/>
<?php echo $this->Html->link(__('New Equipment'), array('action' => 'add'), array('class' => 'btn btn-default')); ?>