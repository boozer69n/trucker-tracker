<div class="equipment form">
<?php echo $this->Form->create('Equipment'); ?>
	<fieldset>
		<legend><?php echo __('Edit Equipment'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('make');
		echo $this->Form->input('model');
		echo $this->Form->input('year');
		echo $this->Form->input('plate');
		echo $this->Form->input('employee_id', array('class' => 'btn btn-default'));
	?>
	</fieldset>
<br/>
    <div class='buttons'>
        <?php
        echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary'));
        echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
        ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>