<!-- File: /app/View/Pages/index.ctp -->

<?php
// Meta Keywords
$this->set(
    'meta_keywords',
    'Home, CSC453, UAT');

// Meta Description
$this->set(
    'meta_description',
    'This is the homepage for CSC453 UAT Trucker Tracker');

// Page Title
$this->set(
    'title_for_layout',
    'Trucker Tracker');

// Page Layout
$this->layout = 'dashboard';
?>

<!-- Home Media -->
<h1 class="page-header">Dashboard</h1>

<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2>$97,689</h2>
        <span class="text-muted">Income to Date</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2>56,891</h2>
        <span class="text-muted">Miles Driven</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2>John Smithers</h2>
        <span class="text-muted">Current Customer</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h2>5/26/15</h2>
        <span class="text-muted">Next Maintenance</span>
    </div>
</div>

<h2 class="sub-header">Recent Maintenance</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Type</th>
            <th>Description</th>
            <th>Cost</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($maintenances as $maintenance): ?>
        <tr>
            <td><?php echo h($maintenance['Maintenance']['id']); ?>&nbsp;</td>
            <td><?php echo h($maintenance['Maintenance']['date']); ?>&nbsp;</td>
            <td><?php echo h($maintenance['MaintenanceType']['name']); ?>&nbsp;</td>
            <td><?php echo h($maintenance['Maintenance']['description']); ?>&nbsp;</td>
            <td>$<?php echo h($maintenance['Maintenance']['cost']); ?>&nbsp;</td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td>33</td>
            <td>1/5/15</td>
            <td>Oil Changed</td>
            <td>That Oil Place</td>
            <td>$150</td>
        </tr>

        </tbody>
    </table>
</div>