<div class="trips form">
<?php echo $this->Form->create('Trip'); ?>
	<fieldset>
		<legend><?php echo __('Add Trip'); ?></legend>
		<?php echo $this->Form->input('employee_id', array('class' => 'btn btn-default dropdown-toggle')); ?>
	</fieldset>
	<div class='buttons'>
	<?php
		echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary'));
		echo $this->Html->link(__('Cancel'), (isset($referer) ? $referer : '/'), array('class' => 'btn btn-default'));
	?>
	</div>
<?php echo $this->Form->end(); ?>
</div>