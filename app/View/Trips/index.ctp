<div class="trips index">
	<h2><?php echo __('Trips'); ?></h2>
	<div class="table-responsive">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Person.name', 'Employee'); ?></th>
			<th>Number of Stops</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($trips as $trip): ?>
	<tr>
		<td><?php echo h($trip['Trip']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($trip['Person']['name'], array('controller' => 'employees', 'action' => 'view', $trip['Employee']['id'])); ?>
		</td>
		<td><?php echo count($trip['Stop']); ?></td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $trip['Trip']['id']), array('class' => 'btn btn-default')); ?>
			<?php if ($is_admin): ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $trip['Trip']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $trip['Trip']['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $trip['Trip']['id'])); ?>
			<?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	</div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<br/>
<?php echo $this->Html->link(__('New Trip'), array('action' => 'add'), array('class' => 'btn btn-default')); ?>