<div class="trips view">
<h2><?php echo __('Trip'); ?></h2>
	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" class="table table-striped">
			<tr>
				<th><?php echo __('Trip ID'); ?></th>
				<th><?php echo __('Employee'); ?></th>
			</tr>
			<tr>
				<td><?php echo h($trip['Trip']['id']); ?></td>

				<td>
					<?php echo $this->Html->link($trip['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $trip['Employee']['id'])); ?>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="related">
	<h3><?php echo __('Related Stops'); ?></h3>
	<?php if (!empty($trip['Stop'])): ?>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Odometer'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($trip['Stop'] as $stop): ?>
		<tr>
			<td><?php echo $stop['id']; ?></td>
			<td><?php echo $this->Time->format($stop['date'], '%B %e, %Y'); ?></td>
			<td><?php echo $stop['description']; ?></td>
			<td><?php echo $this->Number->format($stop['odometer'], array('before' => '','thousands' => ',', 'places' => 0, 'after' => ' Miles')); ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'stops', 'action' => 'view', $stop['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'stops', 'action' => 'edit', $stop['id']), array('class' => 'btn btn-default')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'stops', 'action' => 'delete', $stop['id']), array('class' => 'btn btn-default'), __('Are you sure you want to delete # %s?', $stop['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">		
			<?php echo $this->Html->link(__('New Stop'), array('controller' => 'stops', 'action' => 'add', $trip['Trip']['id']), array('class' => 'btn btn-default')); ?>		
	</div>
</div>
