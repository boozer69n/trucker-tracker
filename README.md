# Trucker Tracker

The Trucker Tracker software will allow truckers to track all aspects of their job through a simple and easy-to-use interface. Once complete, the end user will be able to track their mileage and maintenance for their truck as well as being able to manage their clients and load information. Having access to software like Trucker Tracker will alleviate the need for truckers to revert to the stone age of pen and paper and allow them to focus their time and energy into managing their livelihood. Aside from time savings, information will be kept in a neat orderly fashion which will allow for faster and more accurate use. In addition to this, reports will be available in order to better utilize the information.

### Target Audience
Over the road truckers and fledgling freight company are the primary audience of this software. This software will be created in order to optimize the non-driving aspects of an over the road trucker's job and to ensure accuracy of taxation information.

### Benefit 
Over the road truckers spend a lot of time managing their clientele as well as their truck. 
This takes time away from the profitable act of transporting a load. In order to keep the focus on the profit and not on the day to day operations. Trucker Tracker will also manage and record maintenance of the equipment making sure it is in top working order.


### Version
0.1

### Authors

This project is a collaborative effort of:

* Blake Sutton
* Clinton Booze
* Ian Tyler
* John Daniel

### Technologies
* [CakePHP] - This application was built with CakePHP

[CakePHP]:http://cakephp.org/