import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public class Main {

    private static String baseURL = "http://csc453.computershopware.com/api/";
    private static String secret = "secretkey";
    private static String key = "apikey";

    public static void main(String[] args) {
        // get a handle on the console input
        Scanner scanner = new Scanner(System.in);
        System.out.print("********************************************************\n");
        System.out.print("*   Third Party Trucker Tracker Example Application    *\n");
        System.out.print("********************************************************\n");
        System.out.print("* 1) List all Customers                                *\n");
        System.out.print("* 2) List all Brokers                                  *\n");
        System.out.print("* 3) List Maintenance                                  *\n");
        System.out.print("* 4) Close Application                                 *\n");
        System.out.print("********************************************************\n");

        int option = scanner.nextInt();

        while(option != 4){
            switch(option){
                case 1:
                    listCustomers();
                    break;
                case 2:
                    listBrokers();
                    break;
                case 3:
                    listMaintenance();
                    break;
                default:
                    System.out.println("This is not a valid selection");
            }
            System.out.println("Please enter your next selection or 4 to end.");
            option = scanner.nextInt();
        }
    }

    private static String readUrl(String urlString) {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return "error";
    }

    /**
     * Loop through and print out the customers
     */
    private static void listCustomers() {
        CustomerWrapper container[] = new Gson().fromJson(readUrl(baseURL + "getCustomers/" + secret + "/" + key + ".json"), CustomerWrapper[].class);
        for (int i = 0; i < container.length; i++) {
            System.out.println(container[i].Person.first_name + " " + container[i].Person.last_name);
        }
        if(container.length == 0){
            System.out.println("It does not seem there are any customers");
        }
    }

    /**
     * Loop through and print out the brokers
     */
    private static void listBrokers() {
        CustomerWrapper container[] = new Gson().fromJson(readUrl(baseURL + "getBrokers/" + secret + "/" + key + ".json"), CustomerWrapper[].class);
        for (int i = 0; i < container.length; i++) {
            System.out.println(container[i].Person.first_name + " " + container[i].Person.last_name);
        }
        if(container.length == 0){
            System.out.println("It does not seem there are any brokers");
        }
    }

    /**
     * Loop through and print out the brokers
     */
    private static void listMaintenance() {
        MaintenanceWrapper container[] = new Gson().fromJson(readUrl(baseURL + "getMaintenances/" + secret + "/" + key + ".json"), MaintenanceWrapper[].class);
        for (int i = 0; i < container.length; i++) {
            System.out.println(container[i].Maintenance.description + " " + container[i].Maintenance.expense + " " + container[i].Maintenance.date);
        }
        if(container.length == 0){
            System.out.println("It does not seem there are any maintenances");
        }
    }
}
