/**
 * Created by Blake on 2/5/2015.
 */
public class Person {
    public int id;
    public String first_name;
    public String last_name;
    public int address_id;
    public String home_number;
    public String cell_number;
    public String email;
    public String created;
    public String modified;
}
