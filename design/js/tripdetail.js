// These allow jQueryUI and Bootstrap to play nicely together since they both have a button() function
var tp = $.fn.tooltip.noConflict() // reverts $.fn.button to jqueryui btn
$.fn.tp = tp // assigns bootstrap button functionality to $.fn.btn
var btn = $.fn.button.noConflict() // reverts $.fn.button to jqueryui btn
$.fn.btn = btn // assigns bootstrap button functionality to $.fn.btn

String.prototype.isEmpty = function() {
	return (this.length === 0 || !this.trim());
}

$(document).ready(function() {
	$("#tripdetails-tabs").tabs();
	
	$(".jqtooltip").tooltip({
		items: ".jqtooltip",
		content: function() {
			var id = "#" + $(this).attr("id") + "-tooltip";
			return $(id).html();
		}, 
		position: { my: "left+25", at: "right", collision: "flipfit" }
	});
	
	$(".button").button();
});