// These allow jQueryUI and Bootstrap to play nicely together since they both have a button() function
var btn = $.fn.button.noConflict() // reverts $.fn.button to jqueryui btn
$.fn.btn = btn // assigns bootstrap button functionality to $.fn.btn

String.prototype.isEmpty = function() {
	return (this.length === 0 || !this.trim());
}

// Google services
var geocoder;
var directionDisplay;
var directionServices;
var googleMap;

var map
var formatted_address;
var textbox;
var pickup_location;
var dropoff_location;
var pickup_marker;
var dropoff_marker;
var userAgent = navigator.userAgent;

$(document).ready(function($) {
	map = $("#map");
	if (userAgent.indexOf('iPhone') != -1 || userAgent.indexOf('Android') != -1 ) {
		map.width('100%');
		map.height('100%');
	} else {
		map.width('400px');
		map.height('400px');
	}
	
	geocoder = new google.maps.Geocoder();
	directionServices = new google.maps.DirectionsService();
	directionDisplay = new google.maps.DirectionsRenderer();
	var mapOptions = {
		zoom: 4,
		center: new google.maps.LatLng(50, -120)
	};
	googleMap = new google.maps.Map(map.get(0), mapOptions);
	directionDisplay.setMap(googleMap);

	$("#wizard-tabs").tabs({ activate: tabActivated });
	
	var tabButtons = $("#tab-buttons");
	$("<button id='previous-tab'></button>")
		.appendTo(tabButtons)
		.button({ label: "Back", disabled: true })
		.click(false, selectTab);
	$("<button id='next-tab'></button>")
		.appendTo(tabButtons)
		.button({ label: "Next" })
		.click(true, selectTab);
		
	//$(".verify").hide();
		
	$("#customer-new-address-block").hide();
	$("#broker-new-address-block").hide();

	$(".radios").buttonset();
	$(".radios input[type=radio]").click(radioClick);
	
	$(".full-address").focusout(verifyAddress);
	
	$("#dialog-ask-address").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: {
			"Yes": function() {
				textbox.val(formatted_address);
				$(this).dialog("close");
				textbox.focusout();
			},
			"No": function() {
				textbox = null;
				formatted_address = null;
				$(this).dialog("close");
			}
		}
	});
});

function displayMap() {
	if (pickup_location) {
		if (pickup_marker) {
			pickup_marker.setMap(null);
		}

		pickup_marker = new google.maps.Marker({
			position: pickup_location.geometry.location,
			map: googleMap,
			title: pickup_location.formatted_address,
			animation: google.maps.Animation.DROP
		});
	}
	
	if (dropoff_location) {
		if (dropoff_marker) {
			dropoff_marker.setMap(null);
		}
		
		dropoff_marker = new google.maps.Marker({
			position: dropoff_location.geometry.location,
			map: googleMap,
			title: dropoff_location.formatted_address,
			animation: google.maps.Animation.DROP
		});
	}

	if (pickup_location && dropoff_location) {
		var request = {
			origin: pickup_location.geometry.location,
			destination: dropoff_location.geometry.location,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionServices.route(request, function(response, status) {
			if (google.maps.DirectionsStatus.OK === status) {
				console.log(response);
				directionDisplay.setDirections(response);
				var miles = response.routes[0].legs[0].distance.value * 0.000621371;
				$("#map-distance").text("Distance: " + miles.toFixed(2) + " miles");
			}
		});
	}
}

function verifyAddress(evt) {
	var textBox = $(evt.target);
	var address = textBox.val();
	var temp_address = ("pickup-address" === textBox.attr("id")) ? pickup_location : dropoff_location;
	
	if (!address.isEmpty() && (!temp_address || address.toUpperCase() !== temp_address.formatted_address.toUpperCase())) {
		var verifyText = textBox.next().children().first();
		var verifyGIF = verifyText.next();

		verifyText.text("Verifying Address...");
		verifyGIF.show();

		// Verify address with Google Maps
		geocoder.geocode({ 'address': address }, addressVerifyResults);
	}
}

function addressVerifyResults(results, status) {
	var verifyBlock;
	$(".verify").each(function(index) {
		if ($(this).children().first().next().is(":visible")) {
			verifyBlock = $(this);
		}
	});
	var verifyText = verifyBlock.children().first();
	var verifyGIF = verifyText.next();
	var inputBox = verifyText.parent().prev();
	
	verifyGIF.hide();
	if (google.maps.GeocoderStatus.OK === status) {
		var resultAddress = results[0].formatted_address.toUpperCase();
		var userInput = inputBox.val().toUpperCase();
		if (resultAddress !== userInput) {
			textbox = inputBox;
			formatted_address = results[0].formatted_address;
			
			$("#dialog-possible-address").text(formatted_address);
			$("#dialog-ask-address").dialog("open");
			
			verifyText.text("Failed to validate address. Check your spelling.");
			inputBox.addClass("verify-failed");
			inputBox.removeClass("verified");
		} else {
			verifyText.text("Address validated");
			inputBox.addClass("verified");
			inputBox.removeClass("verify-failed");
			if (inputBox.attr("id") === "pickup-address") {
				pickup_location = results[0];
			} else {
				dropoff_location = results[0];
			}
			displayMap();
		}
	} else {
		verifyText.text("Failed to validate address");
		inputBox.addClass("verify-failed");
		inputBox.removeClass("verified");
	}
}

function radioClick(evt) {
	var button = $(evt.target);
	var isCustomer = button.attr("data-type") == "customer";
	var isNew = button.attr("value") == "new";
	
	if (isCustomer) {
		var existingBlock = $("#select-customer-block");
		var newBlock = $("#customer-new-address-block");
	} else {
		var existingBlock = $("#select-broker-block");
		var newBlock = $("#broker-new-address-block");
	}

	if (isNew) {
		// New Customer/Broker
		newBlock.show(400);
		existingBlock.hide(400);
	} else {
		// Existing Customer/Broker
		newBlock.hide(400);
		existingBlock.show(400);
	}
}

function tabActivated(evt, ui) {
	if ("tab-locations" === ui.newPanel.attr("id")) {
		google.maps.event.trigger(googleMap, 'resize')
	}
}

function selectTab(evt) {
	evt.preventDefault();
	
	var tabs = $("#wizard-tabs");
	var currentTab = tabs.tabs("option", "active");
	
	if (evt.data) { // Next button clicked
		if (currentTab < 2) {
			currentTab++;
			tabs.tabs("option", "hide", { effect: "slide", direction: "left" });
			tabs.tabs("option", "show", { effect: "slide", direction: "right" });
		}
	} else {		// Previous button clicked
		if (currentTab > 0) {
			currentTab--;
			tabs.tabs("option", "hide", { effect: "slide", direction: "right" });
			tabs.tabs("option", "show", { effect: "slide", direction: "left" });
		}
	}
	
	$("#wizard-tabs").tabs("option", "active", currentTab);
	
	if (currentTab == 0) {
		$("#previous-tab").button("disable");
	} else if (currentTab == 2) {
		$("#next-tab").button("option", "label", "Finished");
	} else {
		$("#previous-tab").button("enable");
		$("#next-tab").button("option", "label", "Next");
	}
}