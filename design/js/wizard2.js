// These allow jQueryUI and Bootstrap to play nicely together since they both have a button() function
var btn = $.fn.button.noConflict() // reverts $.fn.button to jqueryui btn
$.fn.btn = btn // assigns bootstrap button functionality to $.fn.btn

String.prototype.isEmpty = function() {
	return (this.length === 0 || !this.trim());
}

var map
var formatted_address;
var textbox;
var pickup_location;
var dropoff_location;
var pickup_marker;
var dropoff_marker;
var userAgent = navigator.userAgent;

$(document).ready(function($) {

	$("#wizard-tabs").tabs({ activate: tabActivated });
	
	var tabButtons = $("#tab-buttons");
	$("<button id='previous-tab'></button>")
		.appendTo(tabButtons)
		.button({ label: "Back", disabled: true })
		.click(false, selectTab);
	$("<button id='next-tab'></button>")
		.appendTo(tabButtons)
		.button({ label: "Next" })
		.click(true, selectTab);
		
	//$(".verify").hide();
		
	$("#customer-new-address-block").hide();

	$(".radios").buttonset();
	$(".radios input[type=radio]").click(radioClick);
	
	$(".full-address").focusout(verifyAddress);
	
	$("#dialog-ask-address").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: {
			"Yes": function() {
				textbox.val(formatted_address);
				$(this).dialog("close");
				textbox.focusout();
			},
			"No": function() {
				textbox = null;
				formatted_address = null;
				$(this).dialog("close");
			}
		}
	});
});

function radioClick(evt) {
	var button = $(evt.target);
	var isCustomer = button.attr("data-type") == "customer";
	var isNew = button.attr("value") == "new";
	
	if (isCustomer) {
		var existingBlock = $("#select-customer-block");
		var newBlock = $("#customer-new-address-block");
	} else {
		var existingBlock = $("#select-broker-block");
		var newBlock = $("#broker-new-address-block");
	}

	if (isNew) {
		// New Customer/Broker
		newBlock.show(400);
		existingBlock.hide(400);
	} else {
		// Existing Customer/Broker
		newBlock.hide(400);
		existingBlock.show(400);
	}
}

function tabActivated(evt, ui) {
	if ("tab-locations" === ui.newPanel.attr("id")) {
		google.maps.event.trigger(googleMap, 'resize')
	}
}

function selectTab(evt) {
	evt.preventDefault();
	
	var tabs = $("#wizard-tabs");
	var currentTab = tabs.tabs("option", "active");
	
	if (evt.data) { // Next button clicked
		if (currentTab < 2) {
			currentTab++;
			tabs.tabs("option", "hide", { effect: "slide", direction: "left" });
			tabs.tabs("option", "show", { effect: "slide", direction: "right" });
		}
	} else {		// Previous button clicked
		if (currentTab > 0) {
			currentTab--;
			tabs.tabs("option", "hide", { effect: "slide", direction: "right" });
			tabs.tabs("option", "show", { effect: "slide", direction: "left" });
		}
	}
	
	$("#wizard-tabs").tabs("option", "active", currentTab);
	
	if (currentTab == 0) {
		$("#previous-tab").button("disable");
	} else if (currentTab == 2) {
		$("#next-tab").button("option", "label", "Finished");
	} else {
		$("#previous-tab").button("enable");
		$("#next-tab").button("option", "label", "Next");
	}
}